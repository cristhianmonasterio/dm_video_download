__author__      = "Cristhian Monasterio"
__copyright__   = "Copyright 2015, Cadetech Engineering DevOps"
__version__     = "1.4"
__email__       = "cmonasterio@cadetech.cl"
__status__      = "production"

import time
import zipfile
import os.path
import argparse
import threading
import subprocess
import ConfigParser
from datetime import *

#argument parser
parser = argparse.ArgumentParser()
parser.add_argument('system_path')
parser.add_argument('directory')

args = parser.parse_args()

name_directory = str(args.directory)
system_path = str(args.system_path)

#read configuration file for get data of nodes
config = ConfigParser.RawConfigParser()
config.read(system_path +'vmeter.config')

#configuration ftp
ftp_server = str(config.get('ftp_cadetech','server'))
ftp_user = str(config.get('ftp_cadetech','user'))
ftp_password = str(config.get('ftp_cadetech','password'))
ftp_path = str(config.get('ftp_cadetech','path'))

#time configuration
time_interval = config.getint('video','time_interval')

#description configuration
name_description = str(config.get('global','name'))
files_rtp = [each for each in sorted(os.listdir(system_path + 'saved/' + name_directory)) if each.endswith('.rtp')]
files_dat = [each for each in sorted(os.listdir(system_path + 'saved/' + name_directory)) if each.endswith('.dat')]

#list for files
list_files_rtp = []
list_files_dat = []

#total time to be sent to concatenate files
total_file = 0

#create lock file
output_locked = open('.locked', "wb")
output_locked.close()

if len(files_rtp) > 0 and len(files_dat) > 0:

    try:
    
      #compress files for backup
      file_compress = system_path + 'saved/' +  name_directory + '/rtp' + '.zip'
      zf = zipfile.ZipFile(file_compress, mode='w')
      
      #files_rtp = files_rtp[1:2]
      for file_rtp in files_rtp:
        zf.write(system_path + 'saved/' +  name_directory + '/' + file_rtp)

    finally:
      zf.close()
    
    output_file = open(name_directory + '.py', "wb")
    
    output_file.write('import subprocess\n')
    output_file.write('import threading\n')
    output_file.write('import os.path\n')
    output_file.write('import ftplib\n\n')
    
    step_mpg = -1 * (((time_interval * 3) - 1))
    step_replay = -1 * (((time_interval * 3) - 2))
        
    output_file.write('#-------------------------------- [PROCESSING SECTION TO AVI VIDEO RTP PACKETS] --------------------------------\n')
    
    #run list for merge replay
    for file in files_rtp:
        name_video = file.split('_')[0] + '_' +file.split('_')[1]

        step_mpg = step_mpg + (time_interval * 3)
        step_replay = step_replay + (time_interval * 3)
        
        total_file = total_file + time_interval
        
        #this function covert to avi
        output_file.write('def covert_to_avi_'+ name_video +'():\n')
        output_file.write('    try:\n')
        output_file.write('        print "\\n> processing the video '+ name_video +'\\n"\n')
        output_file.write('        subprocess.call("ffmpeg -i video.sdp -c copy -f avi -y '+ name_video +'_dump.avi", shell=True)\n')
        output_file.write('    except:\n')
        output_file.write('        pass\n\n')
        
        #this function rtp replay
        output_file.write('def rtp_replay_'+ name_video +'():\n')
        output_file.write('    try:\n')
        output_file.write('        subprocess.call("python rtp_replay.py '+ file +'", shell=True)\n')
        output_file.write('    except:\n')
        output_file.write('        pass\n\n')
        
        #this function executes in a thread packet rtp conversion to avi
        output_file.write('try:\n')
        output_file.write('    t_to_avi_'+ name_video +' = threading.Timer('+ str(step_mpg) +', covert_to_avi_'+ name_video +')\n')
        output_file.write('    t_to_avi_'+ name_video +'.start()\n')
        output_file.write('    t_replay_'+ name_video +' = threading.Timer('+ str(step_replay) +', rtp_replay_'+ name_video +')\n')
        output_file.write('    t_replay_'+ name_video +'.start()\n')
        output_file.write('except:\n')
        output_file.write('    pass\n\n')
        
        list_files_rtp.append(name_video + '_dump.avi')

        #moving file for process
        old_file = system_path + 'saved/' + name_directory + '/' + file
        new_file = file
        os.rename(old_file, new_file)
        
    data_files = ('|'.join(l for l in list_files_rtp))
    
    #move dat file for sampler
    for dat in files_dat:
        #moving file for process
        old_file_dat = system_path + 'saved/' + name_directory + '/' + dat
        new_file_dat = dat
        os.rename(old_file_dat, new_file_dat)
        
    output_file.write('#------------------------------------------ [UNION SECTION AVI VIDEOS] -----------------------------------------\n')

    output_file.write('def join_parts():\n')
    output_file.write('    try:\n')
    output_file.write('        print "\\n> joining the '+ str(data_files) +'\\n"\n')
    output_file.write('        subprocess.call(\'ffmpeg -i concat:"'+ data_files +'" -c copy '+ 'video_' + name_directory +'.avi\', shell=True)\n')
    output_file.write('    except:\n')
    output_file.write('        pass\n\n')
    
    step_replay = step_replay + (time_interval * 3)
    
    output_file.write('try:\n')
    output_file.write('    t_join_parts = threading.Timer('+ str(step_replay) +', join_parts)\n')
    output_file.write('    t_join_parts.start()\n')
    output_file.write('except:\n')
    output_file.write('    pass\n\n')
    
    output_file.write('#------------------------------ [EDITING THE ENTIRE SECTION VIDEO FORMAT CADETECH] -----------------------------\n')
    
    output_file.write('def edit_video():\n')
    output_file.write('    try:\n')
    output_file.write('        print "\\n> editing video ' + name_directory + ' with format cadetech\\n"\n')
    output_file.write('        subprocess.call("python gen_video.py video_' + name_directory + '.avi' +' '+ system_path +' '+ str(total_file) +' '+ str(time_interval) +'", shell=True)\n')
    output_file.write('    except:\n')
    output_file.write('        pass\n\n')
    
    step_replay = step_replay + 1
    
    output_file.write('try:\n')
    output_file.write('    t_edit = threading.Timer('+ str(step_replay) +', edit_video)\n')
    output_file.write('    t_edit.start()\n')
    output_file.write('except:\n')
    output_file.write('    pass\n\n')
    
    output_file.write('#----------------------------------- [SECTION BACKUP VIDEO PROCESSING IN SAVED] --------------------------------\n')
    
    old_final_file = 'video_' + name_directory + '.mp4'
    new_final_file = system_path + 'saved/' + name_directory +'/video_' + name_directory + '.mp4'
    
    old_file_dat = name_directory + '.dat'
    new_file_dat = system_path + 'saved/' + name_directory + '/' + name_directory + '.dat'
    
    old_snapshot_file = 'video_' + name_directory + '.jpeg'
    new_snapshot_file = system_path + 'saved/' + name_directory +'/video_' + name_directory + '.jpeg'
    
    output_file.write('def move_video():\n')
    output_file.write('    os.rename("'+ old_final_file +'", "'+ new_final_file +'")\n')
    
    for dat in files_dat:
        old_file_dat = dat
        new_file_dat = system_path + 'saved/' + name_directory + '/' + dat
        output_file.write('    os.rename("'+ old_file_dat +'", "'+ new_file_dat +'")\n')
    
    output_file.write('    os.rename("'+ old_snapshot_file +'", "'+ new_snapshot_file +'")\n')
    
    output_file.write('    print "\\n> ending process\\n"\n\n')
    step_replay = step_replay + (time_interval * time_interval * 2)

    output_file.write('try:\n')
    output_file.write('    t_move = threading.Timer('+ str(step_replay) +', move_video)\n')
    output_file.write('    t_move.start()\n')
    output_file.write('except:\n')
    output_file.write('    pass\n\n')
    
    output_file.write('#----------------------------------------- [SECTION FOR UPLOAD FILES] -----------------------------------------\n')
    output_file.write('def upload_file():\n')
    output_file.write('    try:\n')
    output_file.write('        print "connecting to ftp server:' + ftp_server + '..."\n')
    output_file.write('        source_file_movie = \''+ new_final_file +'\'\n')
    output_file.write('        file_movie = source_file_movie.split(\'/\')\n')
    output_file.write('        target_file_movie = file_movie[len(file_movie)-1]\n\n')
    output_file.write('        s = ftplib.FTP(\''+ ftp_server +'\', \''+ ftp_user +'\', \''+ ftp_password +'\')\n')
    output_file.write('        f = open(source_file_movie, \'rb\')\n')
    output_file.write('        s.cwd(\''+ ftp_path +'\')\n')
    output_file.write('        s.mkd("'+ name_directory +'")\n')
    output_file.write('        s.cwd(\''+ ftp_path + '/' + name_directory + '\')\n')
    output_file.write('        s.storbinary(\'STOR \' + target_file_movie, f)\n')
    output_file.write('        f.close()\n\n')
    
    output_file.write('        source_file_snapshot = \''+ new_snapshot_file +'\'\n')
    output_file.write('        file_snapshot = source_file_snapshot.split(\'/\')\n')
    output_file.write('        target_snapshot = file_snapshot[len(file_snapshot)-1]\n\n')
    output_file.write('        s = ftplib.FTP(\''+ ftp_server +'\', \''+ ftp_user +'\', \''+ ftp_password +'\')\n')
    output_file.write('        f = open(source_file_snapshot, \'rb\')\n')
    output_file.write('        s.cwd(\''+ ftp_path + '/' + name_directory + '\')\n')
    output_file.write('        s.storbinary(\'STOR \' + target_snapshot, f)\n')
    output_file.write('        f.close()\n\n')
    
    for l in files_dat:
        new_file_dat = system_path + 'saved/'+ name_directory + '/' + l
        output_file.write('        source_file_data = \''+ new_file_dat +'\'\n')
        output_file.write('        file_data = source_file_data.split(\'/\')\n')
        output_file.write('        target_file_data = file_data[len(file_data)-1]\n\n')
        output_file.write('        f = open(source_file_data, \'rb\')\n')
        output_file.write('        s.cwd(\''+ ftp_path + '/' + name_directory + '\')\n')
        output_file.write('        s.storbinary(\'STOR \' + target_file_data, f)\n')
        output_file.write('        f.close()\n\n')
    
    output_file.write('        s.quit()\n')
    output_file.write('    except:\n')
    output_file.write('        pass\n\n')
    
    step_replay = step_replay + 1
    
    output_file.write('try:\n')
    output_file.write('    t_upload = threading.Timer('+ str(step_replay) +', upload_file)\n')
    output_file.write('    t_upload.start()\n')
    output_file.write('except:\n')
    output_file.write('    pass\n\n')
    
    output_file.write('#---------------------------------- [SECTION TEMPORARY REMOVAL OF VIDEO FILES] ---------------------------------\n')
    
    old_file_video = 'video_' + name_directory + '.avi'
    
    output_file.write('def remove_video():\n')
    output_file.write('    os.remove("'+ old_file_video +'")\n')
    
    step_replay = step_replay + (time_interval * time_interval)
    
    output_file.write('try:\n')
    output_file.write('    t_remove_video = threading.Timer('+ str(step_replay) +', remove_video)\n')
    output_file.write('    t_remove_video.start()\n')
    output_file.write('except:\n')
    output_file.write('    pass\n\n')
    
    output_file.write('#---------------------------- [SECTION FOR DISPOSAL OF TEMPORARY FILES PROCESSING] -----------------------------\n')
    
    output_file.write('print "\\n> beging process, at a time temporary files will be deleted\\n"\n')
    
    output_file.write('def delete_files():\n')
    output_file.write('    try:\n')
    
    for l in list_files_rtp:
        output_file.write('        os.remove("'+ l +'")\n')

    for f in files_rtp:
        output_file.write('        os.remove("'+ f +'")\n')
        
    output_file.write('        os.remove("' + name_directory + '.py")\n')
    output_file.write('        os.remove(".locked")\n')
    output_file.write('    except:\n')
    output_file.write('        pass\n\n')
    
    step_replay = step_replay + 1
    
    output_file.write('try:\n')
    output_file.write('    t_delete = threading.Timer('+ str(step_replay) +', delete_files)\n')
    output_file.write('    t_delete.start()\n')
    output_file.write('except:\n')
    output_file.write('    pass\n\n')
    
    output_file.close()
   
else:
    print 'no data to process'