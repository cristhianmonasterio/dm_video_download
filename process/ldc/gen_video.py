__author__      = "Cristhian Monasterio"
__copyright__   = "Copyright 2015, Cadetech Engineering DevOps"
__version__     = "1.4"
__email__       = "cmonasterio@cadetech.cl"
__status__      = "production"

import argparse
import ConfigParser
import os
import fnmatch
import subprocess
import csv
import time
import shutil
from datetime import datetime, timedelta
from PIL import Image, ImageFont, ImageDraw

#set the name of the video and database
parser = argparse.ArgumentParser()
parser.add_argument('video')
parser.add_argument('system_path')
parser.add_argument('lentime')
parser.add_argument('intervaltime')

#set video name
args = parser.parse_args()

name_video_input = str(args.video)
system_path = str(args.system_path)
len_time_video = int(args.lentime)
interval_time = int(args.intervaltime)

#get datas .dat
files_dat = [each for each in sorted(os.listdir(os.getcwd())) if each.endswith('.dat')]

#config
config = ConfigParser.RawConfigParser()
config.read(system_path + 'vmeter.config')

#set video size 
len_time_video = int(args.lentime)
interval_time = int(args.intervaltime)

#global
number_of_nodes = config.getint('global','number_of_nodes')
id_coordinator = str(config.get('global','id_coordinator'))

try:
    ignore_nodes = str(config.get('global','ignore_nodes')).split(",")
except:
    ignore_nodes = []
    
try:
    label_nodes = str(config.get('sampler','label_nodes')).split(",")
except:
    label_nodes = []

#video
logo_video = str(config.get('video','logo'))
name_label = str(config.get('video','title'))

#sampler
colors_sampler = str(config.get('sampler','colors')).split(",")
yrange_min = config.getint('sampler','yrange_min')
yrange_max = config.getint('sampler','yrange_max')

#sets directories operation
dir_frames = '/home/dm_status/dm_video_download/process/ldc/frames'
dir_sampler = '/home/dm_status/dm_video_download/process/ldc/curves'
dir_frames_out = '/home/dm_status/dm_video_download/process/ldc/frames_out'
dir_return = '/home/dm_status/dm_video_download/process/ldc'

#string to time
def str_to_time(time_strFormat):
    if int(len(time_strFormat)) == 20:
        time_strFormat = time_strFormat + '000'

    time_timeFormat = datetime.strptime(time_strFormat, "%Y-%m-%d %H:%M:%S.%f")

    return time_timeFormat

#time to string
def time_to_str(time_timeFormat):
    time_strFormat = datetime.strftime( time_timeFormat,"%Y-%m-%d %H:%M:%S.%f")

    return time_strFormat

def timeCLT_to_timeUTC(timeCLT):
    timeCLT = str_to_time(timeCLT)
    fecha_cambio_hora1 = "2014-04-27 00:00:00.000"
    fecha_cambio_hora1 = str_to_time(fecha_cambio_hora1)
    
    fecha_cambio_hora2 = "2014-09-06 00:00:00.000"
    fecha_cambio_hora2 = str_to_time(fecha_cambio_hora2)
    
    if timeCLT > fecha_cambio_hora1 and timeCLT < fecha_cambio_hora2:
        td = timedelta(hours=+4)
        timeUTC = timeCLT + td

    elif timeCLT >= fecha_cambio_hora2:
        td = timedelta(hours=+3)
        timeUTC = timeCLT + td
        
    else:
        print "this date is not defined in time shift function"
        print "closing program"
        time.sleep(2)
        quit()
        
    return timeUTC

def timeUTC_to_timeCLT(timeUTC):
    fecha_cambio_hora1 = "2014-04-27 04:00:00.000"
    fecha_cambio_hora1 = str_to_time(fecha_cambio_hora1)
    
    fecha_cambio_hora2 = "2014-09-06 04:00:00.000"
    fecha_cambio_hora2 = str_to_time(fecha_cambio_hora2)
    
    if timeUTC > fecha_cambio_hora1 and timeUTC  < fecha_cambio_hora2:
        td = timedelta(hours=+4)
        timeCLT = timeUTC - td

    elif timeUTC >= fecha_cambio_hora2:
        td = timedelta(hours=+3)
        timeCLT = timeUTC - td
        
    else:
        print "this date is not defined in time shift function"
        print "closing program"
        time.sleep(2)
        quit()
        
    return timeCLT

def format_date_for_name_video(name_video_input):
    t_t = name_video_input.split("_")
    t_1 = t_t[1]
    t_2 = t_t[2].split(".")[0]
    t_ini = t_1[0:4] + '-' + t_1[4:6] + '-' + t_1[6:8] + ' ' + t_2[0:2] + ':' + t_2[2:4] + ':' + t_2[4:6] + '.000'
    
    return t_ini

#this function deletes temporary files after processing processing
def remove_files(dir, ext):
    all_filenames = os.listdir(dir)
    
    list_filter_file = []

    for current_filename in all_filenames:
        if fnmatch.fnmatch(current_filename, ext):
            list_filter_file.append(current_filename)
            
    if len(list_filter_file) > 0:   
        os.chdir(dir)
        
        for file in list_filter_file:
            os.remove(file)
            
        print 'remove previous file', ext, 'ok'
        os.chdir(dir_return)
        
    return

#this function generates frames
def gen_frames(name_video_input):
    print 'generating frames...'
    remove_files(dir_frames, '*.*')
    
    dir_name_output = dir_frames + '/image-%3d.jpeg'
    subprocess.call('ffmpeg -i ' + name_video_input + ' -r 8 -f image2 ' + dir_name_output, shell=True)

    return

#this function lists the frames
def list_frame_gen():
    all_filenames = sorted(os.listdir(dir_frames))
    all_filenames_frame = list()

    for current_filename in all_filenames:
        if fnmatch.fnmatch(current_filename, '*.jpeg'):
            all_filenames_frame.append(current_filename)

    print 'amount of catches =', len(all_filenames_frame)

    return all_filenames_frame

#this function generates data files dat measurement
def gen_measurement_dat(list_frame, t_ini):
    print 'generating dat files...'
    print 't_ini = ', t_ini
    
    remove_files(dir_sampler,'*.dat')

    t_ini_time = str_to_time(str(t_ini))
    total_frames = len(list_frame)
 
    paso = (float(len_time_video)/total_frames)
    time_acu = 0

    #get name directory
    datas_file = str(files_dat).split(',')
    datas_file = files_dat[0].split('_')
    name_directory = datas_file[1] + '_' + datas_file[2]

    #run list frames for measurements dat
    for i in range(total_frames):
        #print 'generating measurement curves according to the frame: ' + (str(int(round((i*100)/total_frames)))) + '%...'
        
        i =  i + 1
        time_acu = time_acu + paso
        t_fin_time = t_ini_time + timedelta(seconds=time_acu)
     
        for number in xrange(number_of_nodes):
            node = number + 1
            
            #ingnore nodes
            ignore_node = False
            
            if len(ignore_nodes) > 0:
                for ignore in ignore_nodes:
                    if node == int(ignore):
                        ignore_node = True
                        
            if ignore_node:
                continue
            
            id_node = 'DM-' + id_coordinator + '-' + str((node)).zfill(3)
            name_data_input = id_node + '_' + name_directory
            data_input = open(name_data_input, 'rb')
            data_output = open(dir_sampler + '/' + id_node + '_'+ str(i).zfill(3) + '.dat', 'w')
            reader = csv.reader(data_input)
            
            #run data file measurements
            for row in reader:
                list = ''.join(row).split('|')
                node = str(list[0].strip())
                time = str_to_time(list[1].strip())
                measurement = int(list[2].strip())
                
                if id_node == node and time <= t_fin_time:
                    data_output.write(node + '|' + str(timeUTC_to_timeCLT(time)) + '|' + str(measurement) + '|' + '\n')
                    
            data_output.close()
            data_input.close()

    return

#this function generates curves
def gen_curves(list_frame, t_ini):
    print 'generating curves...'
    remove_files(dir_sampler,'*.png')
    
    t_ini_time = str_to_time(str(t_ini))
    t_ini_time = timeUTC_to_timeCLT(t_ini_time)
    t_fin_time = t_ini_time + timedelta(seconds=len_time_video)
    t_ini_time = time_to_str(t_ini_time)
    t_fin_time = time_to_str(t_fin_time)
    
    proc = subprocess.Popen(['gnuplot'], 
                            shell=True,
                            stdin=subprocess.PIPE,
                            )
    
    proc.stdin.write('tz = -3*3600\n')
    proc.stdin.write('cltz(s) = strftime(fmt, (strptime(fmt,s)+tz))\n')
    proc.stdin.write('set datafile separator \'|\'\n')
    proc.stdin.write('set xlabel \"tiempo\"\n')
    proc.stdin.write('set format x \"%H:%M:%S"\n')
    proc.stdin.write('set xdata time\n')
    proc.stdin.write('set timefmt \"%Y-%m-%d %H:%M:%S\"\n')
    proc.stdin.write('set grid\n')
    proc.stdin.write('set xrange ["' + t_ini_time + '":"' + t_fin_time + '"]\n')
    proc.stdin.write('set xtics  "' + t_ini_time + '",'+ str(interval_time) +',"' + t_fin_time  + '"\n')
    proc.stdin.write('set yrange ['+ str(yrange_min) +':'+ str(yrange_max) +'] \n')
    proc.stdin.write('set key inside left top Left spacing 0.75 horizontal\n')
    proc.stdin.write('set key font "Arial,9"\n')
    proc.stdin.write('set key samplen 2\n')
    proc.stdin.write('set terminal png font \'arial, 14\' fontscale 1.0 size 720, 250\n')
    
    total_frame = len(list_frame)
    list_dat_end = []
    
    for number in xrange(number_of_nodes):
        node = number + 1
        
        #ingnore nodes
        ignore_node = False
        
        if len(ignore_nodes) > 0:
            for ignore in ignore_nodes:
                if node == int(ignore):
                    ignore_node = True
                    
        if ignore_node:
            continue
        
        id_node = 'DM-' + id_coordinator + '-' + str((node)).zfill(3)
        dat_fin = dir_sampler + '/' + id_node + '_' + str(total_frame) + '.dat'
        list_dat_end.append(dat_fin)
        
    paso = (float(len_time_video)/total_frame)
    time_acu = 0
    
    for i in range(total_frame):
        #print 'generating plot curves according frame: ' + (str(int(round((i*100)/total_frame)))) + '%...'
        
        i =  i + 1
        time_acu = time_acu + paso
        
        name_png = dir_sampler + '/curve_%s.png' % str(i).zfill(3)
        proc.stdin.write('set output "' + name_png + '"\n')
        
        last = str(str_to_time(str(t_ini_time)) + timedelta(seconds=time_acu))
        proc.stdin.write('set arrow 101 from "' + last + '" , '+ str(yrange_min) +' to  "' + last + '", '+ str(yrange_max) +' nohead back lc rgb "gray" lw 3 \n')
        
        content_graph = 'plot \'' + dat_fin + '\'' + ' u 2:3 w l lc rgb \"gray\"  notitle, \\'
        
        cont = 0
        
        for number in xrange(number_of_nodes):
            node = number + 1
            
            #ingnore nodes
            ignore_node = False
            
            if len(ignore_nodes) > 0:
                for ignore in ignore_nodes:
                    if node == int(ignore):
                        ignore_node = True
                        
            if ignore_node:
                continue

            id_node = 'DM-' + id_coordinator + '-' + str((number + 1)).zfill(3)
            name_dat = dir_sampler + '/' + id_node + '_' + str(i).zfill(3) + '.dat'
            color = colors_sampler[number].strip()
            
            content_graph = content_graph + '\n\'' + list_dat_end[cont] + '\'' + ' u 2:3 w l lc rgb "gray" lw 0 notitle, \\'
            content_graph = content_graph + '\n\'' + name_dat + '\'' + ' u 2:3 w l lc rgb "'+ str(color).strip() +'" lw 0 title "nodo '+ str(label_nodes[node-1]).strip()  +'"'
            
            cont = cont + 1

            if node < number_of_nodes: 
                content_graph = content_graph + ', \\'
            
        proc.stdin.write(content_graph)
        proc.stdin.write('\n')

    proc.stdin.write('exit\n')
    proc.stdin.write('\n')
    proc.wait()
    
    return

#generate frames
def gen_frame(list_frame, t_ini):
    print 'generating frames output...'
    
    t_ini_utc = str_to_time(str(t_ini))
    t_ini_clt = timeUTC_to_timeCLT(t_ini_utc)
    t_ini_clt = time_to_str(t_ini_clt)
    
    t_ini_clt = t_ini_clt[:19]
    
    logo_cadetech = Image.open('logo_cadetech.png')
    x, y = logo_cadetech.size
    fac = 0.3
    logo_cadetech = logo_cadetech.resize((int(x*fac), int(y*fac)), Image.ANTIALIAS)
    
    logo_dm = Image.open('logo_dm.png')
    x2, y2 = logo_dm.size
    fac2 = 0.6
    logo_dm = logo_dm.resize((int(x2*fac2), int(y2*fac2)), Image.ANTIALIAS)
    
    i = 1
    
    for image in list_frame:
        os.chdir(dir_frames)
        captura = Image.open(image)
        captura = captura.resize((576, 384), Image.ANTIALIAS)
        os.chdir(dir_return)
        
        curva_name = 'curve_%s.png' % str(i).zfill(3)
        i = i + 1
        
        #print 'generating frame output: ' + (str(int(round((i*100)/len(list_frame))))) + '%...'
        
        os.chdir(dir_sampler)
        curva = Image.open(curva_name)
        os.chdir(dir_return)
        
        frame  = Image.new('RGB', (800,600), (255,255,255))
        
        font  = ImageFont.truetype(dir_return + '/arialbd.ttf', 25)
        font2 = ImageFont.truetype(dir_return + '/arialbd.ttf', 18)
    
        draw = ImageDraw.Draw(frame)
    
        frame.paste(captura, (115,50))
        frame.paste(curva, (40,345))
        frame.paste(logo_cadetech, (550,1), mask = logo_cadetech)
        frame.paste(logo_dm, (500,575), mask = logo_dm)
        draw.text((30, 7),  name_label, font = font, fill="black")
        draw.text((30, 575), t_ini_clt , font = font2, fill="black")
        
        os.chdir(dir_frames_out)
        frame.save(image)
        os.chdir(dir_return)
        
    return
        
#correcting firts images
def correct_frame(list_frame):
    print 'correcting firts images'
    i = 1
    
    for image in list_frame:
        if i <= 19:
            shutil.copy(system_path + logo_video, dir_frames_out + '/' + image) 
        else:
            break
        
        i = i + 1
        
#frame to video
def frames_to_video(name_video_input):
    print 'generating video'
    name_video_output = name_video_input[:-4] + '.mp4'
    dir_in  = dir_frames_out + '/image-%3d.jpeg'
    #review
    subprocess.call("ffmpeg -f image2 -r " + str(7.8) + " -i " + dir_in + " -c:v libx264 -s 800x600 " + name_video_output, shell=True)
    
    return

def save_last_frame():
    print 'get last file of frames otputs'
    files = [each for each in sorted(os.listdir(dir_frames_out)) if each.endswith('.jpeg')]
    
    if len(files) > 0:
        file_input = dir_frames_out + '/' + files[len(files)-1]
        file_output = dir_return + '/' + name_video_input.split('.')[0] + '.jpeg'

        if os.access(file_input, os.W_OK):
            os.rename(file_input, file_output)
        
    else:
        print 'error: no files of frames otputs'
    
#execute process
print '\nprocess begin...'

date_initial = format_date_for_name_video(name_video_input)
gen_frames(name_video_input)
list_frame = list_frame_gen()
gen_measurement_dat(list_frame, date_initial)
gen_curves(list_frame, date_initial)
gen_frame(list_frame, date_initial)
correct_frame(list_frame)
frames_to_video(name_video_input)
save_last_frame()

remove_files(dir_frames, '*.*')
remove_files(dir_sampler, '*.*')
remove_files(dir_frames_out, '*.*')

print '\nend process'