__author__      = "Cristhian Monasterio"
__copyright__   = "Copyright 2015, Cadetech Engineering DevOps"
__version__     = "1.4"
__email__       = "cmonasterio@cadetech.cl"
__status__      = "production"

import socket
import binascii
import struct
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('file')

args = parser.parse_args()

#send packet via udp
def process_rtp_packet(output_socket, rtp_packet):
    output_socket.sendto(rtp_packet, ("127.0.0.1", 10002))

#send packet via udp
def process_rtcp_packet(output_socket, rtcp_packet):
    output_socket.sendto(rtcp_packet, ("127.0.0.1", 10003))

input_file = open(args.file, 'rb')

buf_len = 1024
packet_started = False
data_packet = ''
stream_header = ''

output_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#read a chunk of data
while True:
    network_data = input_file.read(buf_len)
    
    if len(network_data) == 0:
        break
    
    bytes_processed = 0
    avail_data_len = len(network_data)
    
    while bytes_processed < avail_data_len:

        if packet_started == True:

            #append the byte to the rtp pack
            data_packet += network_data[bytes_processed]
            #update counters
            read_len -= 1
            bytes_processed += 1
            
            #check if the packet is filled
            if read_len == 0:
                if stream_header[0:2] == struct.pack("BB", 0x24, 0x00):
                    process_rtp_packet(output_socket, data_packet)
                elif stream_header[0:2] == struct.pack("BB", 0x24, 0x00):
                    process_rtcp_packet(output_socket, data_packet)
                packet_started = False
                stream_header = ''
                data_packet = ''
                
        #print "filling stream header data..."        
        elif packet_started == False and len(stream_header) < 4:
            stream_header += network_data[bytes_processed]
            bytes_processed += 1
            
            #check if the stream header is complete
            if len(stream_header) == 4:
                if stream_header[0:2] == struct.pack("BB", 0x24, 0x00) or stream_header[0:2] == struct.pack("BB", 0x24, 0x01):
                    read_len = struct.unpack(">H", stream_header[2:4])[0]
                    packet_started = True
                else:
                    stream_header = ''
        else:
            print "error, discarding byte"
            
            exit()
            bytes_processed += 1

input_file.close()