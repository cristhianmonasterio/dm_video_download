__author__      = "Cristhian Monasterio"
__copyright__   = "Copyright 2015, Cadetech Engineering DevOps"
__version__     = "1.4"
__email__       = "cmonasterio@cadetech.cl"
__status__      = "production"

import os.path
import argparse
import ConfigParser
import subprocess
import time

parser = argparse.ArgumentParser()
parser.add_argument('system_path')
args = parser.parse_args()
system_path = str(args.system_path)

#config parser
config = ConfigParser.RawConfigParser()
config.read(system_path +'vmeter.config')

#time configuration
time_interval = config.getint('video','time_interval')

#this function runs folders saved if there s rtsp file that have not been processed are processed
def run_merge_replay():
	
	if not os.path.exists('.locked'):
	
		directories = [d for d in sorted(os.listdir(system_path + 'saved/')) if os.path.isdir(os.path.join(system_path + 'saved/', d))]
	
		if len(directories) > 0:
	
			for directory in directories:
				try:
					
					files = [each for each in sorted(os.listdir(system_path + 'saved/' + directory)) if each.endswith('.rtp')]
				
					execute = False
					
					if len(files) > 0:
						execute = True
						
					#performs the conversion and subsequent binding of videos
					if execute:
             
						print 'execute packets rtp in ' + system_path + directory + '/'
						subprocess.call('python merge_replay.py ' + system_path + ' ' + directory, shell=True)
						
						time.sleep(1)
						subprocess.call('python ' + directory + '.py', shell=True)
						
				except:
					pass
				
	else:
		
		print 'the system is locked by another process'
		
run_merge_replay()