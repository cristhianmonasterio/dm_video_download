__author__      = "Cristhian Monasterio"
__copyright__   = "Copyright 2015, Cadetech Engineering DevOps"
__version__     = "1.4"
__email__       = "cmonasterio@cadetech.cl"
__status__      = "production"

import os
import re
import sys
import time
import argparse
import datetime
import paramiko
import subprocess
from ConfigParser import SafeConfigParser

parser = SafeConfigParser()
parser.read('manager.ini')

#this is optional, it is to enter a parameter that changes the shift -2 for default
day_shift = None
delta_days = None

try:
  day_shift = str(sys.argv[1]).strip()
  
  if re.match("[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][DN]", day_shift):
    
    current_time = time.strftime("%H:%M:%S")
    param_time = day_shift[0:4] + '-' + day_shift[4:6] + '-' + day_shift[6:8]
    
    if day_shift[8:9] == 'D':
      param_time = param_time + ' 11:00:00.000'
    else:
      param_time = param_time + ' 23:00:00.000'
          
    if current_time >= '11:00:00' and current_time < '23:00:00':
      current_time = '11:00:00.000'
    if current_time >= '23:00:00' and current_time < '11:00:00':
      current_time = '23:00:00.000'
    
    temp_current = str(datetime.datetime.now())
    temp_current = temp_current[0:11] + current_time
    
    temp_current = datetime.datetime.strptime(str(temp_current), "%Y-%m-%d %H:%M:%S.%f")
    param_time = datetime.datetime.strptime(str(param_time), "%Y-%m-%d %H:%M:%S.%f")
    
    delta_time = temp_current - param_time
    delta_time_days = int(delta_time.days)
    delta_time_seconds = (delta_time.seconds)/86400.
    
    delta_days = int((delta_time_days + delta_time_seconds) * 2)
    
  else:
    print 'error: wrong format shift, the format should be 1) aaaammddD or 2)aaaammN; for example: 20160201N'
  
except ValueError:
  sys.exit(0)
  
except:
  delta_days = 1

delta_days = delta_days + 1
  
#this is optional, to enter a parameter is not iterate all the coordinators who are in the configuration file
coordinator_filter = None

try:
  coordinator_filter = abs(int(sys.argv[2]))
  
except ValueError:
  print 'error: wrong format shift, the format should be 1) aaaammddD or 2)aaaammN; for example: 20160201N and 30 (ID Coordinator)'
  sys.exit(0)
except:
  coordinator_filter = None
  
#this is optional, to enter a parameter is not download rtp packets. This is only to generate video
only_generate_video = 0

try:
  only_generate_video = abs(int(sys.argv[3]))
  
except ValueError:
  print 'error: wrong format, the format should be 1) aaaammddD or 2)aaaammN; for example: 20160201N and 30 (ID Coordinator) and 1 (for only to generete video)'
  sys.exit(0)
except:
  only_generate_video = 0
  
coords = []

temp_mea_max = 0
temp_min_max = 0
temp_damage_max = 0

def format_date_for_name_video(name_input):
    t_t = name_input.split("_")
    t_1 = t_t[0]
    t_2 = t_t[1]
    t_ini = str(t_1[0:4] + '-' + t_1[4:6] + '-' + t_1[6:8] + ' ' + t_2[0:2] + ':' + t_2[2:4] + ':' + t_2[4:6] + '.000000')
    t_ini = datetime.datetime.strptime(t_ini, "%Y-%m-%d %H:%M:%S.%f")

    return t_ini

def ssh_connection(path_system, type_system, current_date):    
    count = 0
    hour_back = 100000

    parser = SafeConfigParser()
    parser.read(path_system + 'vmeter.config')
   
    hostname = parser.get('sftp_coordinator', 'hostname')
    port = parser.getint('sftp_coordinator', 'port')
    username = parser.get('sftp_coordinator', 'username')
    password = parser.get('sftp_coordinator', 'password')
    rtp_path = parser.get('sftp_coordinator', 'rtp_path')
    dropbox_path = parser.get('sftp_coordinator', 'dropbox_path')
    number_of_nodes = parser.getint('global', 'number_of_nodes')
    
    list_damage = {}
    list_max_meas = {}
    list_files = {}

    #connection ssh
    print "connecting to %s..." % hostname
    
    try:
        client = paramiko.Transport((hostname, port))
        client.connect(username=username, password=password)
        session = client.open_channel(kind='session')
        
    except paramiko.AuthenticationException:
        print "authentication failed when connecting to %s" % hostname
        pass
        
    except:
        print "couldn't ssh to %s, waiting for it to start" % hostname
        count = count + 1
        time.sleep(2)

    if count == 2:
        print "couldn't connect to %s. giving up" % hostname
        time.sleep(1)
        pass
        
    try:
      session.exec_command('pwd')
      client.set_keepalive(2)

      print "connected successful to %s..." % hostname
      
      #connect to sftp
      sftp = paramiko.SFTPClient.from_transport(client)
      sftp.chdir(dropbox_path)
      
      files_csv = [each for each in sorted(sftp.listdir()) if each.endswith('.csv')]
      
      #read event dropbox folder
      if len(files_csv) > 0:
      
          print 'open file %s...' % files_csv[len(files_csv)-delta_days]
    
          with sftp.open(dropbox_path + files_csv[len(files_csv)-delta_days], 'r') as file:
  
              for line in file:
                  
                  data = line.strip().split('|')
                  folder = data[0]
                  
                  try:
                      force = data[1]
                  except:
                      data = line.strip().split(',')
                      try:
                          force = data[1]
                      except:
                          continue
                  
                  if folder == 'Folder':
                      continue
                  if int(force) > 0:
                      continue
                  
                  time_folder = format_date_for_name_video(folder)
                  time_back = current_date - datetime.timedelta(hours=hour_back)
  
                  if time_folder < time_back:
                      continue
                  
                  i = 6
                  damages = []
                  list_temp = []
                  measurements_max = []
                  measurements_min = []
                  
                  list_temp.append(int(force))
                  list_temp.append(str(data[2]))
                  list_temp.append(int(data[3]))
                  list_temp.append(int(data[4]))
                  list_temp.append(int(data[5]))
                  
                  for number in xrange(number_of_nodes):
                      measurements_max.append(int(data[number + i]))
                      i = i + 1
                      measurements_min.append(int(data[number + i]))
                      damages.append(int(data[(number_of_nodes * 2 ) + (i - 1)]))
                
                  i = 6  
                  for number in xrange(number_of_nodes):
                      list_temp.append(int(data[number + i]))
                      
                  i = 6 + number_of_nodes 
                  for number in xrange(number_of_nodes):
                      list_temp.append(int(data[number + i]))
                      
                  i = 6 + (number_of_nodes * 2) 
                  for number in xrange(number_of_nodes):
                      list_temp.append(int(data[number + i]))
                      
                  sum_damage = sum(damages)
                  max_meas_max = max(measurements_max)
                  
                  list_damage[str(folder)] = sum_damage
                  list_max_meas[str(folder)] = max_meas_max
                  list_files[str(folder)] = list_temp
                    
      else:
          print 'no downloads CSV files to the coordinator'

      #download one event folder
      if len(list_damage) > 0 and len(list_max_meas) > 0:
   
          folder_input = rtp_path
          folder_output = path_system + 'saved/'
          folder_download = None
          temp_damage_max = 0
   
          for damage in list_damage:
              if list_damage[damage] > temp_damage_max:
                  temp_damage_max = list_damage[damage]
                  folder_download = damage
                  
          tem_meas_max = 0
                  
          if temp_damage_max == 0:
              for measurement in list_max_meas:
                  if list_max_meas[measurement] > tem_meas_max:
                      tem_meas_max = list_max_meas[measurement]
                      folder_download = measurement
                      
          if folder_download == None:
              for name_file in list_files:
                folder_download = name_file
       
          #set name folder input and output
          folder_input = str(folder_input) + str(folder_download) + '/'         
          folder_output = str(folder_output) + str(folder_download) + '/'
        
          sftp.chdir(folder_input)
          files_input = sorted(sftp.listdir())
          
          files_input_rtp = [each for each in files_input if each.endswith('.rtp')]

          if len(files_input_rtp) >= 10:
            print 'error: the number of files exceeds rtp excess files over 10 RTP packets, it have '+ str(len(files_input_rtp)) +', please check the status of the situation'
            print 'folder rtp packets: ' + str(folder_input)
            sys.exit(0)
         
          os.mkdir(folder_output, 0755)
          
          #download files
          if len(files_input) > 0:
              for file in files_input:
                  print 'download file %s...' % file
                  sftp.get(folder_input + file, folder_output + file)
                  
          #update indicator dropbox folder
          text_file = ''
          sftp.chdir(dropbox_path)
    
          with sftp.open(dropbox_path + files_csv[len(files_csv)-delta_days], 'r') as file:
              for line in file:
                  data = line.strip().split('|')
                  if data[0] == folder_download:
                      data[1] = 1; data[2] = 100
                  line = '|'.join([str(d) for d in data]) + '\n'
                  text_file = text_file + line
                  
          with sftp.open(dropbox_path + files_csv[len(files_csv)-delta_days], 'w') as file:
              file.write(text_file)
                  
      sftp.close()
      client.close()
      
    except:
        pass
    
def rtp_process(path_system, type_system, current_date):
    
    input_path = '/home/dm_status/dm_video_download/process/' + type_system
    output_path = '/home/dm_status/dm_video_download/'
    
    os.chdir(input_path)
    subprocess.call('python run_merge_replay.py ' + path_system, shell=True)
    os.chdir(output_path)

def clean_workspace():

    path_workspace = '/home/dm_status/dm_video_download/process/ldc/'
    
    files_rtp = [each for each in sorted(os.listdir(path_workspace)) if each.endswith('.rtp')]
    
    if len(files_rtp) > 0:
      for file in files_rtp:
        os.remove(path_workspace + file)
    
    files_dat = [each for each in sorted(os.listdir(path_workspace)) if each.endswith('.dat')]
    
    if len(files_dat) > 0:
      for file in files_dat:
        os.remove(path_workspace + file)
        
    files_avi = [each for each in sorted(os.listdir(path_workspace)) if each.endswith('.avi')]
    
    if len(files_avi) > 0:
      for file in files_avi:
        os.remove(path_workspace + file)
        
    if os.path.exists(path_workspace + '.locked'):
      os.remove(path_workspace + '.locked')

#clean old files
clean_workspace()

#runs events coordinators to download RTP packages 
for section in parser.sections():

    #filter coodinator
    if coordinator_filter > 0:
      if section <> 'dm_coord_%s' % str(coordinator_filter).zfill(3):
        continue

    status = parser.getboolean(section, 'status')
    
    if status:
        #get configuration parameters of coordinator
        current_date = datetime.datetime.now()
        path_system = parser.get(section, 'path_system')
        type_system = parser.get(section, 'type_system')
        
        if only_generate_video == 0:
          ssh_connection(path_system, type_system, current_date)
        
        rtp_process(path_system, type_system, current_date)
    
print 'ready!'